import express from 'express';
import { operationsController } from './controller/OperationsController';
import cors from 'cors';
export const server = express();

server.use(express.json());
server.use(cors());

server.use('/api/operations', operationsController);