import { Router } from "express";
import { OperationsRepository } from "../repository/OperationsRepository";

export const operationsController = Router();

operationsController.get('/', async(req, res) => {
    try {
        let operations;
        if(req.query.search) {
            operations = await OperationsRepository.search(req.query.search);
        }  else {
            operations = await OperationsRepository.getAllOperations();
        }
        res.json(operations);
    } catch (error) {
        console.log(error);
        res.status(500).end();
    }
})

operationsController.get('/:id', async(req, res) => {
    try {
        let operation = await OperationsRepository.findById(req.params.id);
        res.json(operation);
    } catch (error) {
        console.log(error);
        res.status(404).end();
    }
})

operationsController.get('/category/:category', async(req, res) => {
    try {
        let operations = await OperationsRepository.findByCategory(req.params.category);
        res.json(operations);
    } catch (error) {
        console.log(error);
        res.status(500).end();
    }
})

operationsController.get('/month/:date', async (req,res)=>{
    try {
        let operations = await OperationsRepository.getOneMonth(req.params.date);
        res.json(operations);
    } catch (error) {
        console.log(error);
        res.status(500).end();
    }
})

operationsController.delete('/:id', async (req,res)=>{
    try {
        let toDelete = await OperationsRepository.findById(req.params.id);
        if (!toDelete){
            res.status(404);
            return
        }
        await OperationsRepository.delete(toDelete.id);
        res.status(204).end();
    } catch (error) {
        console.log(error);
        res.status(500).end();
    }
})

operationsController.post('/',async(req,res)=>{
    try {
        let id = await OperationsRepository.add(req.body);
        res.status(201).json(await OperationsRepository.findById(id));
    } catch (error) {
        console.log(error);
        res.status(400).end();
    }
})

operationsController.patch('/:id', async (req,res)=>{
    try {
        let data = await OperationsRepository.findById(req.params.id);
        if (!data) {
            res.send(404).end();
            return
        }
        let update = {...data, ...req.body};
        await OperationsRepository.update(update);
        res.json(update);
    } catch (error) {
        console.log(error);
        res.status(400).end();
    }
})

