export class Operation {
    id;
    label;
    amount;
    category;
    date;

    constructor(id, label, amount, category, date){
        this.id=id;
        this.label=label;
        this.amount=amount;
        this.category=category;
        this.date=date;
    }
}