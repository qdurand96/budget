import { Operation } from "../entity/Operation";
import { connection } from "./connection";

export class OperationsRepository {

    static async getAllOperations(){
        let [rows] = await connection.execute(`SELECT * FROM Budget ORDER BY date DESC`);
        let operations = [];
        for (let item of rows) {
            let operation = new Operation(item['id'],item['label'],item['amount'],item['category'],item['date']);
            operations.push(operation);
        }
        return operations;
    };

    static async getOneMonth(date){
        let [rows] = await connection.execute(`SELECT * FROM Budget WHERE MONTH(date)=? ORDER BY date DESC`,[date]);
        let operations = [];
        for (let item of rows) {
            let operation = new Operation(item['id'],item['label'],item['amount'],item['category'],item['date']);
            operations.push(operation);
        }
        return operations;
    };

    static async findById(id){
        let [rows] = await connection.execute(`SELECT * FROM Budget WHERE id=?`, [id]);
        return new Operation(rows[0]['id'],rows[0]['label'],rows[0]['amount'],rows[0]['category'],rows[0]['date'])
    };

    static async search(search){
        let [rows] = await connection.execute(`SELECT * FROM Budget WHERE CONCAT(label,category) LIKE ? ORDER BY date DESC`, ['%'+search+'%']);
        let operations = [];
        for (let item of rows) {
            let operation = new Operation(item['id'],item['label'],item['amount'],item['category'],item['date']);
            operations.push(operation);
        }
        return operations;    
    };

    static async delete(id){
        await connection.execute(`DELETE FROM Budget WHERE id=?`,[id])
    };

    static async add(operation){
        let [data] = await connection.execute('INSERT INTO Budget (label, amount, category, date) VALUES (?,?,?,?)', [operation.label, operation.amount, operation.category, operation.date]);
        return operation.id = data.insertId;
    };

    static async update(operation){
        await connection.execute('UPDATE Budget SET label=?, amount=?, category=?, date=? WHERE id=?', [operation.label, operation.amount, operation.category, operation.date, operation.id]);
    };
}