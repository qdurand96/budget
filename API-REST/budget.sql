DROP DATABASE IF EXISTS `Budget_app`;
CREATE DATABASE `Budget_app`;
USE `Budget_app`;

DROP TABLE IF EXISTS `Budget`;
CREATE TABLE `Budget` (
    `id` int(24) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    `label` VARCHAR(128) NOT NULL,
    `amount` FLOAT NOT NULL,
    `category` VARCHAR(32) NOT NULL,
    `date` DATE NOT NULL
) DEFAULT CHARSET=utf8mb4;

--03
INSERT INTO `Budget` (label, amount, category, date) VALUES (
    'Au Perroquet Bourré', -87.69, 'Loisir', '2021-03-01'
);
INSERT INTO `Budget` (label, amount, category, date) VALUES (
    'Ninkasi Gerland', -24.69, 'Restaurant', '2021-03-04'
);
INSERT INTO `Budget` (label, amount, category, date) VALUES (
    'Leclerc Tignieu', -65.39, 'Courses', '2021-03-15'
);
INSERT INTO `Budget` (label, amount, category, date) VALUES (
    'Citya', -420.00, 'Facture', '2021-03-28'
);
INSERT INTO `Budget` (label, amount, category, date) VALUES (
    'EDF', -80.00, 'Facture', '2021-03-28'
);
INSERT INTO `Budget` (label, amount, category, date) VALUES (
    'Leclerc Tignieu', -135.39, 'Courses', '2021-03-28'
);
--04
INSERT INTO `Budget` (label, amount, category, date) VALUES (
    'La Tamtamerie', -87.69, 'Loisir', '2021-04-01'
);
INSERT INTO `Budget` (label, amount, category, date) VALUES (
    'Bocuse', -125.89, 'Restaurant', '2021-04-04'
);
INSERT INTO `Budget` (label, amount, category, date) VALUES (
    'McDo', -15.39, 'Restaurant', '2021-04-15'
);
INSERT INTO `Budget` (label, amount, category, date) VALUES (
    'Citya', -420.00, 'Facture', '2021-04-28'
);
INSERT INTO `Budget` (label, amount, category, date) VALUES (
    'EDF', -80.00, 'Facture', '2021-04-28'
);
INSERT INTO `Budget` (label, amount, category, date) VALUES (
    'Leclerc Tignieu', -175.39, 'Courses', '2021-04-28'
);
--5
INSERT INTO `Budget` (label, amount, category, date) VALUES (
    'Abreuvoir', -187.69, 'Loisir', '2021-05-01'
);
INSERT INTO `Budget` (label, amount, category, date) VALUES (
    'Sapristi', -34.69, 'Restaurant', '2021-05-04'
);
INSERT INTO `Budget` (label, amount, category, date) VALUES (
    'Intermarché', -95.39, 'Courses', '2021-05-15'
);
INSERT INTO `Budget` (label, amount, category, date) VALUES (
    'Citya', -420.00, 'Facture', '2021-05-28'
);
INSERT INTO `Budget` (label, amount, category, date) VALUES (
    'EDF', -80.00, 'Facture', '2021-05-28'
);
INSERT INTO `Budget` (label, amount, category, date) VALUES (
    'Jacquier', -15.39, 'Restaurant', '2021-05-28'
);