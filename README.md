#Projet Budget

Pour ce projet, j'ai décidé de rester sur quelque chose de simple. J'ai donc prévu une seul table, qui donne le diagramme de classe suivant:

![alt text](Class_diagram.png)

J'ai ensuite codé l'API, avec les controllers nécessaires, et je suis parti sur l'interface. Là aussi, j'ai fais quelque chose de simple, en me concentrant sur le fait d'avoir une structure de projet "propre".

NB: Ce dépot est un monorepository, mais je me suis servi de 2 repos différents pour déployer. Je met celui ci en rendu pour une question de commodité.

**Lien:** <https://budget-app-quentin.netlify.app/>
