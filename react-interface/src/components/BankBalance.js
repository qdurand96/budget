import { useEffect, useState } from "react";

export function BankBalance({operations}){
    
    const[balance, setBalance]=useState(null)

    useEffect(()=>{
        function bankBalance() {
            let balance = 0;
            for (let item of operations){
                balance += item.amount;
            }
            setBalance(balance) 
        }
        bankBalance()
    },[operations])

    return(
        <div className="container-fluid">
            <div className="row">
                <div className="col-md-3"></div>
                <div className="col-md-6 text-center font-weight-bold" style={{fontSize:"40px"}}>
                    <p>Solde : {new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'EUR' }).format(balance)}</p>
                </div>
                <div className="col-md-3"></div>
            </div>
        </div>
    )
}