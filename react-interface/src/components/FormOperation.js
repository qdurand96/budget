import { useState } from "react";

const initialState = {
    label: '',
    amount: '',
    category: '',
    date: ''
}
export function FormOperation({ onSubmit, operation = initialState, isOpen }) {
    const [form, setForm] = useState(operation);
    const [open,setOpen] = useState(isOpen)

    const handleChange = event => {
        setForm({
            ...form,
            [event.target.name]: event.target.value
        });
    }

    function handleSubmit(event) {
        event.preventDefault();
        onSubmit(form);
        setOpen(false);
            // onSubmit({
            //     id:form.id,
            //     label:form.label,
            //     amount:form.amount,
            //     category:form.category,
            //     date: form.date.substring(0, form.date.length - 14)})
    }

    return (
        <>
        {!open &&
        <button onClick={()=>setOpen(true)} className="btn btn-outline-secondary">Ajouter</button>
        }

        {open &&
            <div className='container-fluid'>
            <div className='row'>
                <div className="col-md-3"></div>
                <div className="col-md-6 m-2 py-2 border">
                    <form onSubmit={handleSubmit}>
                        <div className="d-flex justify-content-between border-bottom">
                            <div>
                                <label className="font-weight-bold" htmlFor="date">Date :</label>
                                <input className="ml-1" type="date" name="date" value={form.date} onChange={handleChange} />
                            </div>
                            <button className="btn btn-success mb-1">Confirm</button>
                        </div>
                        <div className='row pt-1'>
                            <div className="col-md-4">
                                <label className="font-weight-bold" htmlFor="label">Label :</label>
                                <input className="w-75" type="text" name="label" value={form.label} onChange={handleChange} />                            </div>
                            <div className="col-md-4">
                                <label className="font-weight-bold" htmlFor="category">Category :</label>
                                <input className="w-75" type="text" name="category" value={form.category} onChange={handleChange} />                            </div>
                            <div className="col-md-4">
                                <label className="font-weight-bold" htmlFor="amount">Amount :</label>
                                <input className="w-75" type="text" name="amount" value={form.amount} onChange={handleChange} />                            </div>
                        </div>
                    </form>
                </div>
                <div className="col-md-3"></div>
            </div>
        </div>
        }
        </>
    )
}