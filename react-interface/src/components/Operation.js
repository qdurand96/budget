import 'bootstrap/dist/css/bootstrap.css';
import { useState } from 'react';
import { FormOperation } from './FormOperation';

export function Operation({ operation, onDelete, onUpdate }) {

    const [editing, setEditing] = useState(false);

    function handleSubmit(formValue) {
        onUpdate(formValue);
        setEditing(false); 
    }

    return (
        <>
            {!editing &&
                <div className='container-fluid'>
                    <div className='row'>
                        <div className="col-md-3"></div>
                        <div className="col-md-6 m-2 py-2 border">
                            <div className="d-flex justify-content-between border-bottom">
                                <p>{new Intl.DateTimeFormat('fr-FR').format(new Date(operation.date))}</p>
                                <div className="mb-1">
                                    <button className='btn btn-primary mr-1' onClick={() => setEditing(true)}>Update</button>
                                    <button className='btn btn-danger' onClick={() => onDelete(operation.id)}>X</button>
                                </div>
                            </div>
                            <div className='row pt-1'>
                                <div className="col-md-4 font-weight-bold">
                                    <p>{operation.label}</p>
                                </div>
                                <div className="col-md-4 text-center">
                                    <p>{operation.category}</p>
                                </div>
                                <div className="col-md-4 text-right font-weight-bold">
                                    <p>{new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'EUR' }).format(operation.amount)}</p>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-3"></div>
                    </div>
                </div>}

                {editing &&
                <FormOperation operation={operation} onSubmit={handleSubmit} isOpen={true}/>}
        </>
    )
}