import { useState } from "react";
import { FaSearch } from 'react-icons/fa';

export function Search({onSearch}) {
    const [search,setSearch] = useState('')

    
    function handleChange (event) {
        setSearch(event.target.value)
    }


    function handleSubmit (event) {
        event.preventDefault();
        onSearch(search);
    }


    return (
        <>
            <form onSubmit={handleSubmit} className="form-inline my-2 my-lg-0">
                <input onChange={handleChange} className="form-control mr-sm-2" type="search" placeholder="Rechercher" aria-label="Search"/>
                <button className="btn btn-outline-secondary my-2 my-sm-0" type="submit"><FaSearch/></button>
            </form>
        </>
        )
}