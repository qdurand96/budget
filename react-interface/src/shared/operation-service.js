import axios from "axios"

export class OperationService {

    static async fetchAll() {
        return axios.get(process.env.REACT_APP_SERVER_URL+'/api/operations');
    }

    static async fetchOneMonth(month) {
        return axios.get(process.env.REACT_APP_SERVER_URL+'/api/operations/month/'+month);
    }

    static async fetchSearch(search) {
        return axios.get(process.env.REACT_APP_SERVER_URL+'/api/operations/?search='+search)
    }

    static async delete(id) {
        axios.delete(process.env.REACT_APP_SERVER_URL+'/api/operations/'+id)
    }

    static async add(operation){
        return axios.post(process.env.REACT_APP_SERVER_URL+'/api/operations', operation)
    }

    static async update(id, operation){
        return axios.patch(process.env.REACT_APP_SERVER_URL+'/api/operations/'+id, operation)
    }


}