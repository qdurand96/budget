import { useEffect, useState } from 'react';
import { Operation } from '../components/Operation';
import { FormOperation } from '../components/FormOperation';
import { OperationService } from '../shared/operation-service';
import { BankBalance } from '../components/BankBalance';
import { DropDown } from '../components/DropDown';
import { Search } from '../components/Search';


export function Home() {

    const [operations, setOperations] = useState([]);

    useEffect(() => {
        fetchOperations();
    }, []);

    async function fetchOperations() {
        let response = await OperationService.fetchAll();
        setOperations(response.data);
    }

    async function addOperation(operation) {
        const response = await OperationService.add(operation);
        console.log(response.data);
        setOperations([...operations, response.data]);
    }

    async function handleDelete(id) {
        await OperationService.delete(id)
        setOperations(operations.filter(item => item.id !== id));
    };

    async function handleUpdate(formValue) {
        const response = await OperationService.update(formValue.id, formValue);

        setOperations(operations.map(item => {
            if (item.id === formValue.id) {
                return response.data;
            }
            return item;
        }
        ))
    }

    async function fetchMonth(month) {
        const response = await OperationService.fetchOneMonth(month);
        setOperations(response.data);
    }

    async function fetchSearched(search) {
        const response = await OperationService.fetchSearch(search);
        setOperations(response.data)
    }


    return (
        <>
            <div className="container-fluid mt-1">
                <div className="row">
                    <div className="col-md-1"></div>
                    <div className="col-md-5">
                        <DropDown selectMonth={fetchMonth} reset={fetchOperations} />
                    </div>
                    <div className="col-md-3 d-flex justify-content-end p-0">
                        <Search onSearch={fetchSearched} />
                    </div>
                    <div className="col-md-3"></div>
                    <div className="col-md-3"></div>
                    <div className="col-md-6">
                        <BankBalance operations={operations} />
                    </div>
                    <div className="col-md-3"></div>
                </div>
                <div className="row d-flex justify-content-center">
                    <FormOperation onSubmit={addOperation} isOpen={false} />
                </div>
            </div>
            {operations.map(operation => <Operation key={operation.id} operation={operation} onDelete={handleDelete} onUpdate={handleUpdate} />)}
        </>
    )
}